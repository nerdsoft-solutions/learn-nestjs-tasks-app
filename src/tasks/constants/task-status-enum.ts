export enum TaskStatusEnum {
    PENDING = "PENDING",
    STARRED = 'STARRED',
    PURCHASED = 'PURCHASED',
    LATER = 'LATER'
}