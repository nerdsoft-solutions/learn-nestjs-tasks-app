import { PartialType } from '@nestjs/mapped-types';
import { IsIn, IsNotEmpty } from 'class-validator';
import { TaskStatusEnum } from '../constants/task-status-enum';

export class CreateTaskDto {
    @IsNotEmpty()
    name: string;

    @IsNotEmpty()
    description: string
}

export class UpdateTaskDto extends PartialType(CreateTaskDto) {
    @IsIn(Object.values(TaskStatusEnum))
    status: TaskStatusEnum
}