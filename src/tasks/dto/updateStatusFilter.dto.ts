import {IsNotEmpty, IsUUID} from "class-validator";

export class UpdateStatusFilterDto {
    @IsUUID()
    id: string;

    @IsNotEmpty()
    key: string;
}
