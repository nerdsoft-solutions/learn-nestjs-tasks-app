import { TaskStatusEnum } from '../constants/task-status-enum';
import { Column, CreateDateColumn, Entity } from 'typeorm';
import { Base } from './base.entity';

@Entity()
export class Tasks extends Base {
    @Column()
    name: string;

    @Column({
        type: 'enum',
        enum: TaskStatusEnum,
        default: TaskStatusEnum.PENDING
    })
    status: TaskStatusEnum;

    @Column()
    description: string;

    @CreateDateColumn()
    createdDate:Date;
}