import { PrimaryGeneratedColumn } from 'typeorm';
import { BaseEntity } from 'typeorm';

export class Base extends BaseEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string;
}