import {
    Body,
    Controller,
    Delete,
    Get,
    HttpCode,
    HttpStatus,
    Param,
    ParseUUIDPipe,
    Patch,
    Post,
    Query,
} from '@nestjs/common';
import { TaskService } from './services/task.service';
import { CreateTaskDto, UpdateTaskDto } from './dto/tasks.dto';
import { Tasks } from './models/Tasks.entity';

@Controller('tasks')
export class TasksController {

    constructor(private taskService: TaskService) {
    }

    @Get()
    getTasks(): Tasks[] {
        return this.taskService.getAllTasks();
    }

    @Post()
    createTask(@Body() task: CreateTaskDto): Tasks {
        console.log(task);
        return this.taskService.addTask(task);
    }

    @Get('search')
    searchByKey(@Query('key') key: string): Array<Tasks> {
        console.log(key);
        return this.taskService.search(key)
    }

    @Patch('/:id')
    updateTask(@Param('id', new ParseUUIDPipe({errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE}))id: string,
               @Body() updateTaskDto: UpdateTaskDto): Tasks {
        console.log(updateTaskDto);
        console.log(id);
        return this.taskService.update(id, updateTaskDto);
    }

    @Delete('reset')
    @HttpCode(HttpStatus.RESET_CONTENT)
    deleteAllTasks(): boolean {
        return this.taskService.clearAll();
    }

    @Delete(':id')
    @HttpCode(HttpStatus.ACCEPTED)
    remove(@Param('id', new ParseUUIDPipe({errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE})) id: string){
        return this.taskService.remove(id);
    }

}
