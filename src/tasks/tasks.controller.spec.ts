import { Test, TestingModule } from '@nestjs/testing';
import { TasksController } from './tasks.controller';
import { TaskService } from './services/task.service';
import { TaskStatusEnum } from './constants/task-status-enum';

describe('TasksController', () => {
  let controller: TasksController;
  let service: TaskService

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TasksController],
      providers: [TaskService]
    }).compile();

    controller = module.get<TasksController>(TasksController);
    service = module.get<TaskService>(TaskService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should retun all the tasks on GET', ()=>{
    expect(controller.getTasks().length).toBe(1);
  })

  it('should add data to the list',()=>{
    const spy = jest.spyOn(service, 'getAllTasks');
    controller.createTask({
      description:'test',
      name: 'test',
    });
    expect(spy.mock).not.toBeNull();
  })

  it('should throw error when any of the param in null',()=>{
    expect(() => controller.createTask({
      description: null,
      name: 'test',
    })).toThrow();
  })
});
