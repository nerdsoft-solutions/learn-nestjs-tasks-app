import { Injectable } from '@nestjs/common';
import { Tasks } from '../models/Tasks.entity';
import { CreateTaskDto, UpdateTaskDto } from '../dto/tasks.dto';
import { v1 as guid } from 'uuid';
import { TaskStatusEnum } from '../constants/task-status-enum';
import { DataNotFoundException } from '../../exceptions/data-not-found-exception';

@Injectable()
export class TaskService {
    private tasks$: Array<Tasks> = [];

    get task(){
        return this.tasks$;
    }

    /**
     * Get all tasks.
     * @return Tasks[]
     * **/
    getAllTasks(): Array<Tasks> {
        return this.tasks$;
    }

    /**
     * Search by key.
     * @param key
     * @return Tasks[]
     * **/
    search(key: string): Array<Tasks> {
        return this.tasks$.filter((value: Tasks) =>
          value.description.toLowerCase().includes(key.toLowerCase()) ||
          value.name.toLowerCase().includes(key.toLowerCase()));
    }

    /**
     * Add tasks to the task list.
     * @Param createTaskDTO
     * @return Tasks
     * **/
    addTask(createTaskDTO: CreateTaskDto): Tasks {
        const {name, description} = createTaskDTO;
        const newTask = {
            id: guid(),
            description,
            name,
            status: TaskStatusEnum.PENDING
        };
        this.tasks$.push(newTask);
        return newTask;
    }

    /**
     * Deletes all the tasks and resets to 0
     * */
    clearAll(): true {
        this.tasks$ = [];
        return true;
    }

    /**
     * Update the task by Id
     * @Param UpdateTaskDto
     * @Return Tasks
     * */
    update(id: string, updateTaskDto: UpdateTaskDto): Tasks {
        return this.tasks$.filter(value => value.id = id).map(value => {
            return {
                ...value,
                ...updateTaskDto
            }
        }).pop();
    }

    /**
     * Update the task by Id
     * @Param UpdateTaskDto
     * @Return Tasks
     * */
    remove(id: string): boolean{
        const needToRemove = this.tasks$.find(value => value.id === id);
        if(needToRemove){
            this.tasks$.splice(this.tasks$.indexOf(needToRemove), 1)
            return true;
        }else {
            throw new DataNotFoundException(`Unable to find the task by id ${id}`);
        }
    }

}