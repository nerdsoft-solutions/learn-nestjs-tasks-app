import { TypeOrmModuleOptions } from '@nestjs/typeorm';

export default () : TypeOrmModuleOptions => ({
  type:'postgres',
  schema: process.env.POSTGRES_SCHEMA,
  host: process.env.POSTGRES_DATABASE_HOST,
  port: parseInt(process.env.POSTGRES_PORT),
  username: process.env.POSTGRES_DATABASE_USERNAME,
  password: process.env.POSTGRES_DATABASE_PASSWORD,
  database: process.env.POSTGRES_DATABASE_NAME,
  logger: 'debug',
  entities: [__dirname + '/../**/*.entity.ts'],
  synchronize: false,
  useUTC: true,
  retryAttempts: 3
})