import { HttpException, HttpStatus } from '@nestjs/common';

export class EnvConfigNotFoundException extends HttpException{
  constructor() {
    super('Environment Config is not defined!', HttpStatus.NOT_IMPLEMENTED);
  }
}