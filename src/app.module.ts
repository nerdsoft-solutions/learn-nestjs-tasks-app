import { Module } from '@nestjs/common';
import { TasksModule } from './tasks/tasks.module';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import postgresConfiguration from './config/postgres-confiugration';

@Module({
  imports: [
    TasksModule,
    TypeOrmModule.forRootAsync({
      useFactory: async () => Object.assign(await postgresConfiguration(),{
        autoLoadEntities: true,
      })
    }),
    ConfigModule.forRoot({isGlobal: true})
  ],
})
export class AppModule {
  constructor() {
    console.log(postgresConfiguration());
  }
}
