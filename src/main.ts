import {NestFactory} from '@nestjs/core';
import {AppModule} from './app.module';
import { Logger, ValidationPipe } from '@nestjs/common';

async function bootstrap() {
    let logger = new Logger('bootstrap');

    const app = await NestFactory.create(AppModule);

    // added validation pipe to configure validations at the app level.
    app.useGlobalPipes(new ValidationPipe({
        // used to ignore any properties that are not defined in DTO Types.
        // But we have to make sure to add notNull/empty validators in the Dto otherwise they will be ignored
        whitelist: true,
        // forbidUnknownValues has to be used with with whitelist.
        forbidUnknownValues: true,
        transform: true
    }))

    const port = process.env.HTTP_PORT || 3000;
    await app.listen(port);

    logger.log(`Application started at port ${port}`);
}

bootstrap();
